provider "aws" {
  region     = "us-west-2"
  access_key = Env.AWS_ACCESS_KEY_ID
  secret_key = Env.AWS_SECRET_ACCESS_KEY
}
